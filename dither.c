#include <stdint.h>
#include <stdio.h>

#define BUFFER_SIZE (1024U)
typedef unsigned char uchar;
void dither_one_channel_16_to_8(int16_t *input, uchar *output, int count, int *memory);

int main(int argc, char *argv[])
{
    FILE *in_fp = stdin;
    FILE *out_fp = stdout;
    int dither_mem = 0;
    size_t elem_read;
    int16_t in_buffer[BUFFER_SIZE];
    uchar out_buffer[BUFFER_SIZE];

    for(;;)
    {
        elem_read = fread(in_buffer, sizeof(int16_t), BUFFER_SIZE, in_fp);
        dither_one_channel_16_to_8(in_buffer, out_buffer, elem_read, &dither_mem);
        fwrite(out_buffer, sizeof(uchar), elem_read, out_fp);

        if(elem_read != BUFFER_SIZE)
        {
            break;
        }
    }
    return 0;
}

/****
* This code will down-convert and dither a 16-bit signed int16_t 
* mono signal into an 8-bit unsigned char signal, using a first 
* order forward-feeding error term dither.
**/
void dither_one_channel_16_to_8(int16_t *input, uchar *output, int count, int *memory)
{
  int m = *memory;
  while( count-- > 0 ) {
    int i = *input++;
    i += m;
    int j = i + 32768 - 128;
    uchar o;
    if( j < 0 ) {
      o = 0;
    }
    else if( j > 65535 ) {
      o = 255;
    }
    else {
      o = (uchar)((j>>8)&0xff);
    }
    m = ((j-32768+128)-i);
    *output++ = o;
  }
  *memory = m;
}

