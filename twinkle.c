#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <math.h>

#define BUFFER_SIZE 1024

typedef struct Ramper_s
{
    float state;
    float target;
    float coeff;
} Ramper;

typedef struct Oscillator_s
{
    Ramper   ramper;
    unsigned phase;
    unsigned step;
    unsigned stepFactor;
    short (*callback)(unsigned);
} Oscillator;

Oscillator* new_Oscillator(unsigned int sampleRate, short (*callback)(unsigned));
void free_Oscillator(Oscillator* p);
void process_Oscillator(Oscillator* p, short* y, size_t n);

double midi_to_freq(uint8_t code);
short oscillator_func(unsigned p);
short sawtooth_func(unsigned phase);

int main()
{
    static short buffer[BUFFER_SIZE];
    static uint8_t notes[] = {
    /*  C   C   G   G   A   A   G   SIL */
        60, 60, 67, 67, 69, 69, 67, '\0',
    /*  F   F   E   E   D   D   C   SIL */
        65, 65, 64, 64, 62, 62, 60, '\0',
    /*  G   G   F   F   E   E   D   SIL */
        67, 67, 65, 65, 64, 64, 62, '\0',
    /*  G   G   F   F   E   E   D   SIL */
        67, 67, 65, 65, 64, 64, 62, '\0',
    /*  C   C   G   G   A   A   G   SIL */
        60, 60, 67, 67, 69, 69, 67, '\0',
    /*  F   F   E   E   D   D   C   SIL */
        65, 65, 64, 64, 62, 62, 60, '\0',
    };
    static const size_t c_len = sizeof(notes)/sizeof(uint8_t);
    static const unsigned int SR = 44100;
    int i,n;
    uint8_t* nptr = notes;

    /* allocating oscillator */
    Oscillator* osc = new_Oscillator(SR, oscillator_func);
#if 0
    for(i = 0; i < c_len; ++i)
    {
        if(*nptr)
        {
            /* setting oscillator frequency */
            double freq = midi_to_freq(*nptr) * ((double)0xffffffff);
            long long int step = (osc->stepFactor * freq);
            osc->step = (int)(step >> 32);

            n = 0;

            while(n < 10)
            {
                process_Oscillator(osc, buffer, BUFFER_SIZE);
                fwrite(buffer, sizeof(short), BUFFER_SIZE, stdout);
                ++n;
            }
            n = 0, memset(buffer, 0, sizeof(short) * BUFFER_SIZE);
            while(n < 10)
            {
                fwrite(buffer, sizeof(short), BUFFER_SIZE, stdout);
                ++n;
            }
        }
        else
        {
            n = 0, memset(buffer, 0, sizeof(short) * BUFFER_SIZE);
            while(n < 20)
            {
                fwrite(buffer, sizeof(short), BUFFER_SIZE, stdout);
                ++n;
            }
        }
        nptr++;
    }
#else
    for(i = 0; i < c_len; ++i)
    {
        if(*nptr)
        {
            /* setting oscillator frequency */
            double freq = midi_to_freq(*nptr) * ((double)0xffffffff);
            long long int step = (osc->stepFactor * freq);
            osc->step = (int)(step >> 32);

            n = 0;
            osc->ramper.target = 1.0f;

            while(n < 10)
            {
                process_Oscillator(osc, buffer, BUFFER_SIZE);
                fwrite(buffer, sizeof(short), BUFFER_SIZE, stdout);
                ++n;
            }
            n = 0;
            osc->ramper.target = 0.0f;

            /* memset(buffer, 0, sizeof(short) * BUFFER_SIZE); */
            while(n < 10)
            {
                process_Oscillator(osc, buffer, BUFFER_SIZE);
                fwrite(buffer, sizeof(short), BUFFER_SIZE, stdout);
                ++n;
            }
        }
        else
        {
            n = 0;
            osc->ramper.target = 0.0f;

            /* memset(buffer, 0, sizeof(short) * BUFFER_SIZE); */
            while(n < 10)
            {
                process_Oscillator(osc, buffer, BUFFER_SIZE);
                fwrite(buffer, sizeof(short), BUFFER_SIZE, stdout);
                ++n;
            }
        }
        nptr++;
    }
#endif

    free_Oscillator(osc);

    return 0;
}

short sawtooth_func(unsigned phase)
{
    int out = phase+phase;
    out = out - 0x7fffffff;
    out >>= 17;
    return out;
}
// Precise method which guarantees v = v1 when t = 1.
inline float lin_interp(float v0, float v1, float t) {
  return (1-t)*v0 + t*v1;
}
short oscillator_func(unsigned phase)
{
#if 0
    static const char LUT_SIN[257] = {
      128,131,134,137,140,143,146,149,152,156,159,162,165,168,171,174,
      176,179,182,185,188,191,193,196,199,201,204,206,209,211,213,216,
      218,220,222,224,226,228,230,232,234,236,237,239,240,242,243,245,
      246,247,248,249,250,251,252,252,253,254,254,255,255,255,255,255,
      255,255,255,255,255,255,254,254,253,252,252,251,250,249,248,247,
      246,245,243,242,240,239,237,236,234,232,230,228,226,224,222,220,
      218,216,213,211,209,206,204,201,199,196,193,191,188,185,182,179,
      176,174,171,168,165,162,159,156,152,149,146,143,140,137,134,131,
      128,124,121,118,115,112,109,106,103,99, 96, 93, 90, 87, 84, 81,
      79, 76, 73, 70, 67, 64, 62, 59, 56, 54, 51, 49, 46, 44, 42, 39,
      37, 35, 33, 31, 29, 27, 25, 23, 21, 19, 18, 16, 15, 13, 12, 10,
      9,  8,  7,  6,  5,  4,  3,  3,  2,  1,  1,  0,  0,  0,  0,  0,
      0,  0,  0,  0,  0,  0,  1,  1,  2,  3,  3,  4,  5,  6,  7,  8,
      9,  10, 12, 13, 15, 16, 18, 19, 21, 23, 25, 27, 29, 31, 33, 35,
      37, 39, 42, 44, 46, 49, 51, 54, 56, 59, 62, 64, 67, 70, 73, 76,
      79, 81, 84, 87, 90, 93, 96, 99, 103,106,109,112,115,118,121,124,
      128
    };
    const unsigned int lut_radix = 8U;
    const unsigned int lut_size  = 256U;
    const unsigned int lut_mask  = lut_size - 1U;
    const unsigned int int_bits  = (sizeof(unsigned int) * CHAR_BIT);
    /****
     * Linear interpolating with wavetable:
     * v = v0 * (1 - alpha) + v1 * alpha
     * v0 := 'lower bound'
     * v1 := 'upper bound'
     ***/
    unsigned int lower_bound_index = phase >> (int_bits - lut_radix);
    int alpha_0 = lut_mask & phase;
    int alpha_1 = lut_mask - alpha_0;
    int v0 = LUT_SIN[lower_bound_index    ];
    int v1 = LUT_SIN[lower_bound_index + 1];
    int v  = v0 * alpha_1 + v1 * alpha_0;
    v += rand() & 0xf;
    return v >> 2;
#else
#define TABLE_SIZE (256)
    static const float LUT_SIN[TABLE_SIZE + 1] = {
      1.0000000e+000,  9.9969882e-001,  9.9879546e-001,  9.9729046e-001,
      9.9518473e-001,  9.9247953e-001,  9.8917651e-001,  9.8527764e-001,
      9.8078528e-001,  9.7570213e-001,  9.7003125e-001,  9.6377607e-001,
      9.5694034e-001,  9.4952818e-001,  9.4154407e-001,  9.3299280e-001,
      9.2387953e-001,  9.1420976e-001,  9.0398929e-001,  8.9322430e-001,
      8.8192126e-001,  8.7008699e-001,  8.5772861e-001,  8.4485357e-001,
      8.3146961e-001,  8.1758481e-001,  8.0320753e-001,  7.8834643e-001,
      7.7301045e-001,  7.5720885e-001,  7.4095113e-001,  7.2424708e-001,
      7.0710678e-001,  6.8954054e-001,  6.7155895e-001,  6.5317284e-001,
      6.3439328e-001,  6.1523159e-001,  5.9569930e-001,  5.7580819e-001,
      5.5557023e-001,  5.3499762e-001,  5.1410274e-001,  4.9289819e-001,
      4.7139674e-001,  4.4961133e-001,  4.2755509e-001,  4.0524131e-001,
      3.8268343e-001,  3.5989504e-001,  3.3688985e-001,  3.1368174e-001,
      2.9028468e-001,  2.6671276e-001,  2.4298018e-001,  2.1910124e-001,
      1.9509032e-001,  1.7096189e-001,  1.4673047e-001,  1.2241068e-001,
      9.8017140e-002,  7.3564564e-002,  4.9067674e-002,  2.4541229e-002,
      6.1232340e-017, -2.4541229e-002, -4.9067674e-002, -7.3564564e-002,
     -9.8017140e-002, -1.2241068e-001, -1.4673047e-001, -1.7096189e-001,
     -1.9509032e-001, -2.1910124e-001, -2.4298018e-001, -2.6671276e-001,
     -2.9028468e-001, -3.1368174e-001, -3.3688985e-001, -3.5989504e-001,
     -3.8268343e-001, -4.0524131e-001, -4.2755509e-001, -4.4961133e-001,
     -4.7139674e-001, -4.9289819e-001, -5.1410274e-001, -5.3499762e-001,
     -5.5557023e-001, -5.7580819e-001, -5.9569930e-001, -6.1523159e-001,
     -6.3439328e-001, -6.5317284e-001, -6.7155895e-001, -6.8954054e-001,
     -7.0710678e-001, -7.2424708e-001, -7.4095113e-001, -7.5720885e-001,
     -7.7301045e-001, -7.8834643e-001, -8.0320753e-001, -8.1758481e-001,
     -8.3146961e-001, -8.4485357e-001, -8.5772861e-001, -8.7008699e-001,
     -8.8192126e-001, -8.9322430e-001, -9.0398929e-001, -9.1420976e-001,
     -9.2387953e-001, -9.3299280e-001, -9.4154407e-001, -9.4952818e-001,
     -9.5694034e-001, -9.6377607e-001, -9.7003125e-001, -9.7570213e-001,
     -9.8078528e-001, -9.8527764e-001, -9.8917651e-001, -9.9247953e-001,
     -9.9518473e-001, -9.9729046e-001, -9.9879546e-001, -9.9969882e-001,
     -1.0000000e+000, -9.9969882e-001, -9.9879546e-001, -9.9729046e-001,
     -9.9518473e-001, -9.9247953e-001, -9.8917651e-001, -9.8527764e-001,
     -9.8078528e-001, -9.7570213e-001, -9.7003125e-001, -9.6377607e-001,
     -9.5694034e-001, -9.4952818e-001, -9.4154407e-001, -9.3299280e-001,
     -9.2387953e-001, -9.1420976e-001, -9.0398929e-001, -8.9322430e-001,
     -8.8192126e-001, -8.7008699e-001, -8.5772861e-001, -8.4485357e-001,
     -8.3146961e-001, -8.1758481e-001, -8.0320753e-001, -7.8834643e-001,
     -7.7301045e-001, -7.5720885e-001, -7.4095113e-001, -7.2424708e-001,
     -7.0710678e-001, -6.8954054e-001, -6.7155895e-001, -6.5317284e-001,
     -6.3439328e-001, -6.1523159e-001, -5.9569930e-001, -5.7580819e-001,
     -5.5557023e-001, -5.3499762e-001, -5.1410274e-001, -4.9289819e-001,
     -4.7139674e-001, -4.4961133e-001, -4.2755509e-001, -4.0524131e-001,
     -3.8268343e-001, -3.5989504e-001, -3.3688985e-001, -3.1368174e-001,
     -2.9028468e-001, -2.6671276e-001, -2.4298018e-001, -2.1910124e-001,
     -1.9509032e-001, -1.7096189e-001, -1.4673047e-001, -1.2241068e-001,
     -9.8017140e-002, -7.3564564e-002, -4.9067674e-002, -2.4541229e-002,
     -1.8369702e-016,  2.4541229e-002,  4.9067674e-002,  7.3564564e-002,
      9.8017140e-002,  1.2241068e-001,  1.4673047e-001,  1.7096189e-001,
      1.9509032e-001,  2.1910124e-001,  2.4298018e-001,  2.6671276e-001,
      2.9028468e-001,  3.1368174e-001,  3.3688985e-001,  3.5989504e-001,
      3.8268343e-001,  4.0524131e-001,  4.2755509e-001,  4.4961133e-001,
      4.7139674e-001,  4.9289819e-001,  5.1410274e-001,  5.3499762e-001,
      5.5557023e-001,  5.7580819e-001,  5.9569930e-001,  6.1523159e-001,
      6.3439328e-001,  6.5317284e-001,  6.7155895e-001,  6.8954054e-001,
      7.0710678e-001,  7.2424708e-001,  7.4095113e-001,  7.5720885e-001,
      7.7301045e-001,  7.8834643e-001,  8.0320753e-001,  8.1758481e-001,
      8.3146961e-001,  8.4485357e-001,  8.5772861e-001,  8.7008699e-001,
      8.8192126e-001,  8.9322430e-001,  9.0398929e-001,  9.1420976e-001,
      9.2387953e-001,  9.3299280e-001,  9.4154407e-001,  9.4952818e-001,
      9.5694034e-001,  9.6377607e-001,  9.7003125e-001,  9.7570213e-001,
      9.8078528e-001,  9.8527764e-001,  9.8917651e-001,  9.9247953e-001,
      9.9518473e-001,  9.9729046e-001,  9.9879546e-001,  9.9969882e-001,
      1.0000000e+000
    };
    /****
     * Linear interpolating with wavetable:
     * v = v0 * (1 - alpha) + v1 * alpha
     * v0 := 'lower bound'
     * v1 := 'upper bound'
     ***/
    unsigned int lower_bound_index = phase >> (32 - 8);
    static const float scale = 1.0f / ((float)(UINT_MAX) + 1.0f);
    float fphase = (float)(phase) * scale;
    float t = fphase - (((float)lower_bound_index)*scale);

    float v0 = LUT_SIN[lower_bound_index    ];
    float v1 = LUT_SIN[lower_bound_index + 1];

    float v = (v0*t) + ((1.0f - t)*v1) ;

    v *= 0.5;
    v = (v >= +1.0f) ? +1.0f: v;
    v = (v <= -1.0f) ? -1.0f: v;
    v = 32767.0f * v;

    int16_t r = (int16_t)v;
    return v;
#endif
}

double midi_to_freq(uint8_t code)
{
    static const double f0 = 440.0;
    double p = (double)code;
    double f = f0 * pow(2.0, (p - 69.0) / 12.0);
    return f;
}

Oscillator* new_Oscillator(unsigned int sampleRate, short (*callback)(unsigned))
{
    Oscillator* osc = NULL;
    if((osc = (Oscillator*)  malloc( sizeof(Oscillator) )) == NULL)
        return NULL;
    osc->phase = 0;
    osc->stepFactor = ((unsigned)(-1))/sampleRate;
    osc->step = 0;
    osc->callback = callback;
    osc->ramper.state = 1.0f;
    osc->ramper.target = 1.0f;
    osc->ramper.coeff = 0.002;
    return osc;
}
void free_Oscillator(Oscillator* p)
{
    if(p) free(p);
}

void process_Oscillator(Oscillator* osc, short* y, size_t n)
{
    size_t i = 0;
    short (*func)(unsigned) = osc->callback;
    unsigned p = osc->phase;
    unsigned s = osc->step;

    float gain = osc->ramper.state;
    float target = osc->ramper.target;
    float alpha = osc->ramper.coeff;

    for(i; i < n; ++i)
    {
        gain = osc->ramper.state;
        target = osc->ramper.target;
        alpha = osc->ramper.coeff;

        /* if ramper is steady */
        if(fabsf(gain - target) <= 1.0e-4)
        {
            y[i] = gain * func(p);
            p += s;
        }
        else

        {
            /* use gain */
            y[i] = gain * func(p);
            p += s;
            /* update gain */
            gain = alpha * target + (1.0f - alpha) * gain;
            /* update state */
            osc->ramper.state = gain;
        }
    }
    osc->phase = p;
}

