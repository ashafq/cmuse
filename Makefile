
BIN := twinkle
SPEC := twinkle.png
SRC := twinkle.c
OBJ := $(SRC:.c=.o)
CC  := gcc
CFLAGS := -O0 -g
LFLAGS := -lm
SOX    := $(shell which sox)
PLAY   := $(shell which play)
SOX_FLAGS := -t raw -c 1 -r 44100 -e signed-integer -b 16
SPEC_FLAGS   := -n spectrogram -m -l

.PHONY: default all clean play

default: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^ $(LFLAGS)

%.o:%.c
	$(CC) $(CFLAGS) -c -o $@ $<

play: $(BIN)
	@if [ -n $(PLAY) ]; \
	then \
		echo "Playing..."; \
		$(PWD)/$(BIN) | $(PLAY) $(SOX_FLAGS) - ;\
	else \
		echo "You do not have sox package installed"; \
	fi;

DITHER = dither
SOX_FLAGS_DITHER = -t raw -c 1 -r 44100 -e unsigned-integer -b 8
play8: $(DITHER)
	@if [ -n $(PLAY) ]; \
	then \
		echo "Playing..."; \
		$(PWD)/$(BIN) | $(PWD)/$(DITHER) | $(PLAY) $(SOX_FLAGS_DITHER) - ;\
	else \
		echo "You do not have sox package installed"; \
	fi;

$(DITHER): dither.c
	$(CC) -o $@ $< -lm

graph: $(SPEC)
$(SPEC): $(BIN)
	@if [ -n $(SOX) ]; \
	then \
		echo "Plotting spectrogram to file..."; \
		$(PWD)/$(BIN) | $(SOX) $(SOX_FLAGS) - $(SPEC_FLAGS) -o $@;\
		echo "Spectrogram saved as $@";\
	else \
		echo "You do not have sox package installed"; \
	fi;

CLEANUP_LIST = $(OBJ) $(BIN) $(SPEC) $(DITHER)
clean:
	@echo "Removing: $(CLEANUP_LIST)"; \
	rm -f $(CLEANUP_LIST)


